import "../sass/style.scss";
require('jquery-zoom/jquery.zoom.min');
require('./window-event/ready.js');
require('./window-event/load.js');
require('./window-event/resize.js');
require('./window-event/scroll.js');
