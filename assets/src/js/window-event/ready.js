// Windows Ready Handler

(function ($) {

  $(document).ready(function () {

    $(".project-image img").click(function () {
      const imgUrl = $(this).attr('src');
      const zooImgUrl = $(this).attr('data-zoom-src');
      const videoUrl = $(this).attr('data-video-url');
      const modal = $("#imageModalCenter");
      const modalBody = $("#imageModalCenter .modal-body");
      if (videoUrl) {
        modal.addClass('videoModal');
        modalBody.html('<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" width="560" height="315" src="' + videoUrl + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>');
        modal.modal('show');
      } else {
        modal.removeClass('videoModal');
        modalBody.html('<figure id="imgFigure" class="zoom"><img src="' + imgUrl + '" /></figure>');
        modal.modal('show');

        $('#imgFigure').zoom({
          url: zooImgUrl,
          callback: function () {
            console.log('aaa');
          }
        });
      }
    });

    $('.navbar-toggler').click(function() {
      $('#main-navbar').toggleClass('show');
    })

  });

}(jQuery));
