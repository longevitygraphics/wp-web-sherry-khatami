// Window Resize Handler

(function ($) {

  var imagesLoaded = require('imagesloaded');

  function initializeMarginTop() {
    let scrollbarWidth = window.innerWidth - document.body.offsetWidth;
    let viewportWidth = $('.horizontal').innerWidth();
    let horizontalBodyWidth = $(".horizontal__body").width();
    let marginTop = (horizontalBodyWidth - viewportWidth) + window.innerHeight - scrollbarWidth - 1;
    //debugger
    $('.horizonSpace').get(0).style.setProperty('--marginTop', marginTop + 'px');
  }

  $(window).on('resize', function () {
    if (window.innerWidth >= 1200) {
      initializeMarginTop();
    }
  });

  $(document).ready(function () {

    if (window.innerWidth >= 1200) {
      initializeMarginTop();

      let imagesWidth = 0;
      //check if the page is single project
      const pageIsProject = $("body").hasClass('page-template-project');
      console.log(pageIsProject);
      const imagePadding = parseInt($('.project-image').css('padding-right'));
      //console.log(imagePadding);
      imagesLoaded("#horizontal-body-project",function () {
        //calculate the width of images
        $("#horizontal-body-project img").each(function (index) {
          imagesWidth += $(this).width();
          imagesWidth += imagePadding;
        });

        console.log(imagesWidth);
        $("#horizontal-body-project").width(imagesWidth);
        initializeMarginTop();
      })
      /*$("#horizontal-body-project").imagesLoaded()
        .always(function (instance) {
          console.log('all images loaded');
        })
        .done(function (instance) {
          console.log('all images successfully loaded');
        })
        .fail(function () {
          console.log('all images loaded, at least one is broken');
        })*/
    }
  });

}(jQuery));
