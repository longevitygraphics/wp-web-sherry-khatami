// Window Scroll Handler

(function ($) {

  function makeCellsSquare() {
    const cellHeight = $(".cell").first().height();
    $(".grid8x5 .column").css('min-width', cellHeight);
  }

  const $horizontal = $(".horizontal");
    const $isFixedImage = document.querySelector(".project-image.is-fixed");
    let imageOffset, imageWidth, $isFixedImageNExtSibling;

  if (window.innerWidth >= 1200) {
    makeCellsSquare();
    
    if ($isFixedImage) {
      $isFixedImageNExtSibling = $($isFixedImage).next();
      imageOffset = $isFixedImage.offsetLeft;
      imageWidth = $($isFixedImage).width();
    }

  }
  $(window).on('scroll', function () {
    const $horizontal = $(".horizontal");
    if (window.innerWidth >= 1200){
      console.log($(window).width())
      $horizontal.get(0).style.setProperty('--scrollY', window.scrollY + 'px');
      //$horizontal.css('--scrollY', window.scrollY + 'px');
      //console.log(window.scrollY);
      // console.log(imageOffset, window.scrollY, imageWidth);
  
      if (window.scrollY >= imageOffset) {
        $($isFixedImage).addClass('fix-it').css('left', window.scrollY);
        //$isFixedImageNExtSibling.css('padding-left', imageWidth);
      } else {
        $($isFixedImage).removeClass('fix-it');
        //$isFixedImageNExtSibling.css('padding-left', 0);
      }
    }
  })


}(jQuery));
