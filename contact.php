<?php
/**
 * Template Name: Contact page template
 */

get_header(); ?>

<main>
	<div class="contact-content">
		<div class="container-fluid">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
