<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action( 'document_start' ); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action( 'html_class' ); ?>>
<head>
	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
	<![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php do_action( 'wp_header' ); ?>
	<?php wp_head(); ?>
	<script src="http://localhost:35729/livereload.js"></script>

</head>

<body <?php body_class(); ?>>

<?php do_action( 'wp_body_start' ); ?>


<div id="page" class="site">
	<header id="site-header">
		<div class="header-main">
			<?php get_template_part( "/templates/template-parts/header/main-nav" ); ?>
		</div>
	</header><!-- #masthead -->
	<div id="site-content" role="main">
		<?php do_action( 'wp_content_top' ); ?>
