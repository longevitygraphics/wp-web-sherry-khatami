<?php
/**
 * Template Name: Home page template
 */

get_header(); ?>

<main class="horizontal" style="--scrollY: 0px;">

	<div class="horizontal__body grid8x5" id="horizontal-body">
		<?php

		$columns = get_field('projects');
		//print_r($columns);
		$projects = array();
		$active_cells = array();

		function getRandomCell($active_cells_for_column){
			$rand = rand(2, 5);
			if(isset($active_cells_for_column) and !empty($active_cells_for_column)){
				if(in_array($rand, $active_cells_for_column))
				$rand = rand(2, 5);
			}
			return $rand;
		}

		foreach ($columns as $column_key => $column) {
			$active_cells[$column_key] =[];
			if ($column['grid_column']) {
				foreach ($column['grid_column'] as $cell) {
					//getRandomCell($active_cells[$column_key]);
					$random_cell = getRandomCell($active_cells[$column_key]);;
					$cell['cell'] = $random_cell;
					$projects[] = $cell;
					$active_cells[$column_key][] = $cell['cell'];
				}
			}else{
				$active_cells[$column_key] =[];
			}
		}
		shuffle($projects);
		$mobile_projects = $projects;

		//print_r($active_cells);
		//print_r($projects);

		$j = 0;
		$columnsCount = count($active_cells);
		while ($j < $columnsCount) {
			$column_active_cells = $active_cells[$j];
			?>
			<div class="column d-none d-xl-flex">
				<?php
				$row_counter = 5;
				for ($i = 1; $i <= $row_counter; $i++):
					$cell_image = null;
					$cell_icon = null;
					$cell_title = null;
					$cell_link = "#";
					$project = '';
					//print_r($cell);
					if (in_array($i, $column_active_cells)) {
						$project = array_pop($projects);
						$cell_image = $project['image'];
						$cell_icon = $project['icon'];
						$cell_title = $project['title'];
						$cell_link = $project['link'];
					}
					?>
					<div class="cell">
						<?php if ($cell_title): ?>
							<a href="<?php echo $cell_link ?>">
								<img class="icon" alt="<?php echo $cell_icon['alt'] ?>"
									 title="<?php echo $cell_icon['title'] ?>"
									 src="<?php echo $cell_icon['url'] ?>"/>
								<img class="image" alt="<?php echo $cell_image['alt'] ?>"
									 title="<?php echo $cell_image['title'] ?>"
									 src="<?php echo $cell_image['url'] ?>"/>

								<h3 class="project-title"><?php echo $cell_title ?></h3>
							</a>
						<?php endif; ?>
					</div>

					<?php

					?>

				<?php

				endfor;
				?>
			</div>
			<?php

			$j++;
		}

		foreach ($mobile_projects as $project) {
			$cell_image = $project['image'];
			$cell_icon = $project['icon'];
			$cell_link = $project['link'];
			?>
			<a class="cell m-column-item d-xl-none" href="<?php echo $cell_link; ?>">
				<h3 class="project-title"><?php echo $project['title'] ?></h3>
				<img alt="<?php echo $cell_icon['alt'] ?>"
					 title="<?php echo $cell_icon['title'] ?>"
					 src="<?php echo $cell_icon['url'] ?>"/>
			</a>
			<?php
		}
		?>


		<!--<div class="column">-->
		<?php //flexible_layout(); ?>
	</div>


</main>
<div class="horizonSpace" style="--marginTop:20px;"></div>

<?php get_footer(); ?>
