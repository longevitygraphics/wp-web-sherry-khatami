<?php
//print_r(get_sub_field('column'));

$column = get_sub_field( 'column' );
?>
<div class="column d-none d-xl-flex">
	<?php
	$row_counter    = 5;
	for ( $i = 1; $i <= $row_counter; $i ++ ):
		$cell_image = null;
		$cell_title = null;
		$cell_link  = "#";
		if ( $column ):
			foreach ( $column as $cell ) :
				if ( $cell['row'] == $i ) {
					$cell_image = $cell['image'];
					$cell_title = $cell['title'];
					$cell_link  = $cell['project_link'];
					break;
				}
			endforeach;
		endif;
		?>
		<div class="cell" style="<?php echo $cell_image ? 'background-image:url(' . $cell_image['url'] . ')' : '' ?>">
			<?php if ( $cell_title ): ?>
				<a href="<?php echo $cell_link ?>">
					<h3 class="project-title"><?php echo $cell_title ?></h3>
				</a>
			<?php endif; ?>
		</div>
	<?php

	endfor;
	?>
</div>

<?php
if ( $column ):
	foreach ( $column as $cell ) :
		$cell_image = $cell['image'];
		$cell_link = $cell['project_link'];
		?>
		<a class="cell m-column-item d-xl-none" href="<?php echo $cell_link; ?>">
			<h3 class="project-title"><?php echo $cell['title'] ?></h3>
			<img alt="<?php echo $cell_image['alt'] ?>" title="<?php echo $cell_image['title'] ?>"
			     src="<?php echo $cell_image['url'] ?>"/>
		</a>
	<?php
	endforeach;
endif;
?>


