<?php
/**
 * Template Name: Project page template
 */
get_header(); ?>


<main class='pb-5'>
	<div class="container">
		<?php  
			$title = get_field('title');
			$description = get_field('description');
		?>
		<div class="project-title mb-4"><?php the_field( 'title' ); ?></div>
		<?php if ( have_rows( 'multi_images' ) ) : ?>
			<div class="project-images mb-4">
			<?php while ( have_rows( 'multi_images' ) ) : the_row(); ?>
				<div class="project-img">
				<?php
					$image_id = get_sub_field('image')['ID'];
					$size = "full";
					echo wp_get_attachment_image( $image_id, $size );
				?>
				</div>
			<?php endwhile; ?>
			</div>
		<?php endif; ?>
		<div class="project-description">
			<?php the_field( 'description' ); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>
