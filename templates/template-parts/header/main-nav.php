<div class="main-navigation">
	<nav class="navbar">
		<a class="navbar-brand mr-auto mr-lg-0" href="/">
			<?php echo site_logo(); ?>
			<?php //echo str_replace(' ', '<br/>', get_bloginfo('name')); ?>
		</a>

		<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button" data-toggle="offcanvas">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</button>

		<!-- Main Menu  -->
		<div id="main-navbar" >
			<?php

			$mainMenu = array(
				// 'menu'              => 'menu-1',
				'theme_location' => 'top-nav',
				'depth'          => 2,
				'container'      => false,
				/*'container' => 'div',
				'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
				'container_id' => 'main-navbar',*/
				'menu_class'     => 'navbar-nav ml-auto',
				'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
				'walker'         => new WP_Bootstrap_Navwalker()
			);
			wp_nav_menu( $mainMenu );

			?>
		</div>
	</nav>
</div>
