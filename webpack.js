// ----- Path Configurations
var config = {
    sassPath            : './assets/src/sass',
    jsPath              : './assets/src/js',
    imagesrc            : './assets/src/images/'
};



//require('slick-carousel');
require(config.sassPath + '/style.scss');
require(config.jsPath + '/script.js');

